//
//  Database.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/29/20.
//

import UIKit

class Database {
    
    static func createCities() -> [String] {
        ["Санкт-Петербург", "Москва", "Калининград"]
    }
    
    static func createShops() -> [Shop] {
        // MARK: - First shop
        
        let firstShop = Shop(
            name: "Adidas",
            description: "Один из первых магазинов Adidas в Санкт-Петербурге. Большой магазин с разнобразными товарами, есть интернет-помощник.",
            image: Asset.Adidas.adidasLogo.image,
            coordinate: ShopAddress(
                stingAddress: "Санкт-Петербург, ул. Ефимова, 3",
                latitude: 59.925078,
                longitude: 30.319192
            ),
            contactNumber: "+7 812 407-71-29",
            mapPin: Asset.Adidas.adidasMapPin.image,
            product: [
                Product(
                    name: "Название_1",
                    image: Asset.Adidas.adidasFirstProduct.image,
                    cost: "150 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_2",
                    image: Asset.Adidas.adidasSecondProduct.image,
                    cost: "250 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_3",
                    image: Asset.Adidas.adidasThirdProduct.image,
                    cost: "125 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_4",
                    image: Asset.Adidas.adidasFourthProduct.image,
                    cost: "115 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_5",
                    image: Asset.Adidas.adidasFivethProduct.image,
                    cost: "170 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_6",
                    image: Asset.Adidas.adidasSixthProduct.image,
                    cost: "93 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: true
                ),
                Product(
                    name: "Название_7",
                    image: Asset.Adidas.adidasSeventhProduct.image,
                    cost: "741 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_8",
                    image: Asset.Adidas.adidasEightProduct.image,
                    cost: "252 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: true
                ),
                Product(
                    name: "Название_9",
                    image: Asset.Adidas.adidasNinethProduct.image,
                    cost: "151 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                )
            ]
        )
        
        // MARK: - Second shop
        
        let secondShop = Shop(
            name: "Nike",
            description: "Один из первых магазинов Nike в Санкт-Петербурге. Большой магазин с разнобразными товарами, есть интернет-помощник.",
            image: Asset.Nike.nikeLogo.image,
            coordinate: ShopAddress(
                stingAddress: "Санкт-Петербург, Невский пр., 35",
                latitude: 59.933863,
                longitude: 30.334210
            ),
            contactNumber: "+7 892 107-70-15",
            mapPin: Asset.Nike.nikeMapPin.image,
            product: [
                Product(
                    name: "Название_1",
                    image: Asset.Nike.nikeFirstProduct.image,
                    cost: "150 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_2",
                    image: Asset.Nike.nikeSecondProduct.image,
                    cost: "250 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_3",
                    image: Asset.Nike.nikeThirdProduct.image,
                    cost: "125 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_4",
                    image: Asset.Nike.nikeFourthProduct.image,
                    cost: "115 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_5",
                    image: Asset.Nike.nikeFivethProduct.image,
                    cost: "170 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_6",
                    image: Asset.Nike.nikeSixthProduct.image,
                    cost: "93 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: true
                ),
                Product(
                    name: "Название_7",
                    image: Asset.Nike.nikeSeventhProduct.image,
                    cost: "741 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_8",
                    image: Asset.Nike.nikeEightProduct.image,
                    cost: "252 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: true
                ),
                Product(
                    name: "Название_9",
                    image: Asset.Nike.nikeNinethProduct.image,
                    cost: "151 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                )
            ]
        )
        
        // MARK: - Third shop
        
        let thirdShop = Shop(
            name: "Gucci",
            description: "Один из первых магазинов Gucci в Санкт-Петербурге. Большой магазин с разнобразными товарами, есть интернет-помощник.",
            image: Asset.Gucci.gucciLogo.image,
            coordinate: ShopAddress(
                stingAddress: "Санкт-Петербург, ул. Варшавская",
                latitude: 59.877902,
                longitude: 30.313022
            ),
            contactNumber: "+7 435 131-25-17",
            mapPin: Asset.Gucci.gucciMapPin.image,
            product: [
                Product(
                    name: "Название_1",
                    image: Asset.Gucci.gucciFirstProduct.image,
                    cost: "1500 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_2",
                    image: Asset.Gucci.gucciSecondProduct.image,
                    cost: "2500 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_3",
                    image: Asset.Gucci.gucciThirdProduct.image,
                    cost: "1250 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_4",
                    image: Asset.Gucci.gucciFourthProduct.image,
                    cost: "1150 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_5",
                    image: Asset.Gucci.gucciFivethProduct.image,
                    cost: "1700 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_6",
                    image: Asset.Gucci.gucciSixthProduct.image,
                    cost: "930 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: true
                ),
                Product(
                    name: "Название_7",
                    image: Asset.Gucci.gucciSeventhProduct.image,
                    cost: "7410 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                ),
                Product(
                    name: "Название_8",
                    image: Asset.Gucci.gucciEightProduct.image,
                    cost: "2502 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: true
                ),
                Product(
                    name: "Название_9",
                    image: Asset.Gucci.gucciNinethProduct.image,
                    cost: "1510 $",
                    description: "Это лучшая вещь из лучшего магазина. И тут просто описание для того, чтобы был хоть какой-то текст. Поэтому прочитайте и улыбнитесь:)",
                    isOnSale: false
                )
            ]
        )
        
        return [firstShop, secondShop, thirdShop]
    }
}
