//
//  Product.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/29/20.
//

import UIKit

struct Product {
    var name: String
    var image: UIImage
    var cost: String
    var description: String
    var isOnSale: Bool
}
