//
//  Shop.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/29/20.
//

import UIKit

struct Shop {
    var name: String
    var description: String
    var image: UIImage
    var coordinate: ShopAddress
    var contactNumber: String
    var mapPin: UIImage
    var product: [Product]?
}
