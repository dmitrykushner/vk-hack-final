//
//  ShopAddress.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/29/20.
//

import Foundation

struct ShopAddress {
    var stingAddress: String
    var latitude: Double
    var longitude: Double    
}
