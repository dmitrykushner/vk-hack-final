//
//  MainAssembly.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

final class CitiesAssembly {
    init() {
    }
    
    func assembly(delegate: CitiesViewControllerDelegate) -> CitiesViewController {
        let model = CitiesModel()
        let viewController = CitiesViewController()
        viewController.model = model
        viewController.delegate = delegate
        model.responder = viewController
        return viewController
    }
}
