//
//  MainView.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import SnapKit
import GoogleMaps
import Then
import UIKit

protocol CitiesViewResponder: AnyObject {
    // Выбран город
    func tap(city: String)
}

final class CitiesView: UIView {
    // MARK: - Public Properties
    
    weak var responder: CitiesViewResponder?
    
    // MARK: - Subview Properties
    
    private lazy var tableView = UITableView().then {
        $0.delegate = self
        $0.dataSource = self
    }
    
    // MARK: - Private Properties
    
    private var cities = [String]()
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        backgroundColor = .white
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(tableView)
    }
    
    private func makeConstraints() {
        tableView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
    }
    
    func configure(with cities: [String]) {
        self.cities = cities
        tableView.reloadData()
    }
}

// MARK: - UITableViewDelegate

extension CitiesView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        responder?.tap(city: cities[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView()
    }
}

// MARK: - UITableViewDataSource

extension CitiesView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        50
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cities.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = cities[indexPath.row]
        return cell
    }
}
