//
//  MainModel.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

protocol CitiesModelChangeProtocol: AnyObject {
    func citiesLoaded(cities: [String])
}

final class CitiesModel {
    
    weak var responder: CitiesModelChangeProtocol?
    
    var cities = [String]()
    
    init() {
    }
    
    func loadCities() {
        cities = Database.createCities()
        responder?.citiesLoaded(cities: cities)
    }
}
