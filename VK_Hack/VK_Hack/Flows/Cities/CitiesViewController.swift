//
//  MainViewController.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import UIKit

protocol CitiesViewControllerDelegate: AnyObject {
    func cityChoosed(city: String)
}

final class CitiesViewController: UIViewController {
    // MARK: - Subview Properties
    
    private var contentView = CitiesView()
    
    var model: CitiesModel? = nil
    weak var delegate: CitiesViewControllerDelegate?
    
    override func loadView() {
        super.loadView()
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.responder = self
        navigationController?.setNavigationBarHidden(true, animated: true)
        model?.loadCities()
    }
    
}

// MARK: - CitiesModelChangeProtocol

extension CitiesViewController: CitiesModelChangeProtocol {
    func citiesLoaded(cities: [String]) {
        contentView.configure(with: cities)
    }
}

// MARK: - CitiesViewResponder

extension CitiesViewController: CitiesViewResponder {
    func tap(city: String) {
        dismiss(animated: true) {
            self.delegate?.cityChoosed(city: city)
        }
    }
}
