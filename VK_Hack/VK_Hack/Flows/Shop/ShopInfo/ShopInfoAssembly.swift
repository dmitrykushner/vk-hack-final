//
//  MainAssembly.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

final class ShopInfoAssembly {
    init() {
    }
    
    func assembly(shop: Shop) -> ShopInfoViewController {
        let model = ShopInfoModel(shop: shop)
        let viewController = ShopInfoViewController()
        viewController.model = model
        model.responder = viewController
        return viewController
    }
}
