//
//  MainModel.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

protocol ShopInfoModelChangeProtocol: AnyObject {
    func showShopInfo(shop: Shop)
}

final class ShopInfoModel {
    
    weak var responder: ShopInfoModelChangeProtocol?
    var shop: Shop?
    
    init(shop: Shop) {
        self.shop = shop
    }
    
    func viewLoaded() {
        guard let shop = shop else { return }
        responder?.showShopInfo(shop: shop)
    }
}
