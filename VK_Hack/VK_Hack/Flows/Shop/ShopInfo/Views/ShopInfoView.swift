//
//  MainView.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import SnapKit
import GoogleMaps
import Then
import UIKit

protocol ShopInfoViewResponder: AnyObject {
    // Тап на кнопку бэк
    func tapBackButton()
    // Тап на продукт
    func tap(product: Product)
}

final class ShopInfoView: UIView {
    // MARK: - Public Properties
    
    weak var responder: ShopInfoViewResponder?
    
    // MARK: - Subview Properties
    
    private lazy var navigationBarView = UIView()

    private lazy var titleLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        $0.textAlignment = .center
    }

    private lazy var backButton = UIButton().then {
        $0.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        $0.setImage(Asset.backArrow.image, for: .normal)
    }

    private lazy var separatorLineView = UIView().then { $0.backgroundColor = .lightGray }
    
    private lazy var collectionView = UICollectionView(frame: frame, collectionViewLayout: UICollectionViewFlowLayout()).then {
        $0.register(ShopInfoCollectionViewCell.self, forCellWithReuseIdentifier: "ShopInfoCollectionViewCell")
        $0.backgroundColor = .clear
        $0.delegate = self
        $0.dataSource = self
    }
    
    // MARK: - Private Properties
    
    private var shop: Shop?
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        backgroundColor = .white
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(navigationBarView)
        navigationBarView.addSubview(backButton)
        navigationBarView.addSubview(titleLabel)
        navigationBarView.addSubview(separatorLineView)
        addSubview(collectionView)
    }
    
    private func makeConstraints() {
        navigationBarView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(44)
        }
        backButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(8)
            make.leading.equalToSuperview().inset(16)
            make.width.equalTo(28)
        }
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(backButton.snp.trailing).offset(12)
            make.center.equalToSuperview()
        }
        separatorLineView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.height.equalTo(0.5)
        }
        collectionView.snp.makeConstraints { make in
            make.top.equalTo(navigationBarView.snp.bottom)
            make.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    @objc private func backAction() {
        responder?.tapBackButton()
    }
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout

extension ShopInfoView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        shop?.product?.count ?? .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard
            let cell = collectionView
                .dequeueReusableCell(withReuseIdentifier: "ShopInfoCollectionViewCell", for: indexPath) as? ShopInfoCollectionViewCell,
            let model = shop?.product?[indexPath.row]
        else { return UICollectionViewCell() }
        
        cell.configure(with: .init(image: model.image, name: model.name, cost: model.cost))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let product = shop?.product?[indexPath.row] else { return }
        responder?.tap(product: product)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowayout = collectionViewLayout as? UICollectionViewFlowLayout
        let space: CGFloat = (flowayout?.minimumInteritemSpacing ?? 0.0) + (flowayout?.sectionInset.left ?? 0.0) + (flowayout?.sectionInset.right ?? 0.0)
        let width: CGFloat = (collectionView.frame.size.width - space) / 2.1
        let height: CGFloat = (collectionView.frame.size.height - space) / 3.6
        return CGSize(width: width, height: height)
    }
}

// MARK: - Configurable

extension ShopInfoView {
    struct ViewModel {
        var shop: Shop
    }
    
    func configure(with viewModel: ViewModel) {
        self.shop = viewModel.shop
        titleLabel.text = viewModel.shop.name
        collectionView.reloadData()
    }
}
