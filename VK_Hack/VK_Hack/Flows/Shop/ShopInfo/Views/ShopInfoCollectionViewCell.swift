//
//  ShopInfoCollectionViewCell.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/30/20.
//

import UIKit

final class ShopInfoCollectionViewCell: UICollectionViewCell {
    // MARK: - Subview Properties
    
    private lazy var productImageView = UIImageView().then {
        $0.contentMode = .scaleAspectFit
        $0.layer.cornerRadius = 8
    }
    
    private lazy var nameLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 13)
        $0.textColor = .lightGray
        $0.textAlignment = .center
    }
    
    private lazy var costLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 16)
        $0.textAlignment = .center
    }
    
    // MARK: - UIView

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(productImageView)
        addSubview(nameLabel)
        addSubview(costLabel)
    }
    
    private func makeConstraints() {
        productImageView.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(16)
            make.centerX.equalToSuperview()
            make.size.equalTo(0.65 * frame.height)
        }
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(productImageView.snp.bottom).offset(8)
            make.centerX.equalToSuperview()
            make.leading.greaterThanOrEqualToSuperview().inset(8)
        }
        costLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(4)
            make.bottom.equalToSuperview().inset(8)
            make.centerX.equalToSuperview()
            make.leading.greaterThanOrEqualToSuperview().inset(8)
        }
    }
}

extension ShopInfoCollectionViewCell {
    struct ViewModel {
        var image: UIImage
        var name: String
        var cost: String
    }
    
    func configure(with viewModel: ViewModel) {
        productImageView.image = viewModel.image
        nameLabel.text = viewModel.name
        costLabel.text = viewModel.cost
    }
}
