//
//  MainViewController.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import UIKit

protocol ShopInfoViewControllerDelegate: AnyObject {
    func cityChoosed(city: String)
}

final class ShopInfoViewController: UIViewController {
    // MARK: - Subview Properties
    
    private var contentView = ShopInfoView()
    
    var model: ShopInfoModel? = nil
    
    override func loadView() {
        super.loadView()
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.responder = self
        model?.viewLoaded()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
}

// MARK: - ShopInfoModelChangeProtocol

extension ShopInfoViewController: ShopInfoModelChangeProtocol {
    func showShopInfo(shop: Shop) {
        contentView.configure(with: .init(shop: shop))
    }
}

// MARK: - ShopInfoViewResponder

extension ShopInfoViewController: ShopInfoViewResponder {
    func tap(product: Product) {
        let viewController = ProductInfoAssembly().assembly(product: product)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tapBackButton() {
        navigationController?.popViewController(animated: true)
    }
}
