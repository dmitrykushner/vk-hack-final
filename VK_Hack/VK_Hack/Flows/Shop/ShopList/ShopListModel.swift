//
//  MainModel.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

protocol ShopListModelChangeProtocol: AnyObject {
    func loaded(shops: [Shop])
}

final class ShopListModel {
    
    weak var responder: ShopListModelChangeProtocol?
    
    init() {
    }
    
    func loadShops() {
        responder?.loaded(shops: Database.createShops())
    }
}
