//
//  ShopListTableViewCell.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/30/20.
//

import UIKit

final class ShopListTableViewCell: UITableViewCell {
    // MARK: - Subview Properties
    
    private lazy var logoImageView = UIImageView().then { $0.contentMode = .scaleAspectFit }
    
    private lazy var titleLabel = UILabel().then { $0.font = UIFont.systemFont(ofSize: 16, weight: .semibold) }

    // MARK: - UITableViewCell

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        commonInit()
    }

    required init?(coder: NSCoder) { nil }

    // MARK: - Private Methods

    private func commonInit() {
        addSubviews()
        makeConstraints()
    }

    private func addSubviews() {
        addSubview(logoImageView)
        addSubview(titleLabel)
    }

    private func makeConstraints() {
        logoImageView.snp.makeConstraints { make in
            make.top.bottom.leading.equalToSuperview().inset(16)
            make.size.equalTo(44)
        }
        titleLabel.snp.makeConstraints { make in
            make.top.bottom.equalTo(logoImageView)
            make.leading.equalTo(logoImageView.snp.trailing).offset(16)
            make.trailing.equalToSuperview().inset(16)
        }
    }
}

extension ShopListTableViewCell {
    struct ViewModel {
        var image: UIImage
        var title: String
    }
    
    func configure(with viewModel: ViewModel) {
        logoImageView.image = viewModel.image
        titleLabel.text = viewModel.title
    }
}
