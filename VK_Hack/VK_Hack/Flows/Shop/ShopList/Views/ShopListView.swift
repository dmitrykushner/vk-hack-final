//
//  MainView.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import SnapKit
import GoogleMaps
import Then
import UIKit

protocol ShopListViewResponder: AnyObject {
    // Тап на кнопку бэк
    func tapBackButton()
    // Тап на магазин
    func tap(shop: Shop)
}

final class ShopListView: UIView {
    // MARK: - Public Properties
    
    weak var responder: ShopListViewResponder?
    
    // MARK: - Subview Properties
    
    private lazy var navigationBarView = UIView()

    private lazy var titleLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        $0.text = "Магазины"
        $0.textAlignment = .center
    }

    private lazy var backButton = UIButton().then {
        $0.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        $0.setImage(Asset.backArrow.image, for: .normal)
    }

    private lazy var separatorLineView = UIView().then { $0.backgroundColor = .lightGray }
    
    private lazy var tableView = UITableView().then {
        $0.delegate = self
        $0.dataSource = self
        $0.register(ShopListTableViewCell.self, forCellReuseIdentifier: "ShopListTableViewCell")
        $0.separatorStyle = .none
    }
    
    // MARK: - Private Properties
    
    var shops = [Shop]()
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        backgroundColor = .white
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(navigationBarView)
        navigationBarView.addSubview(backButton)
        navigationBarView.addSubview(titleLabel)
        navigationBarView.addSubview(separatorLineView)
        addSubview(tableView)
    }
    
    private func makeConstraints() {
        navigationBarView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(44)
        }
        backButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(8)
            make.leading.equalToSuperview().inset(16)
            make.width.equalTo(28)
        }
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(backButton.snp.trailing).offset(12)
            make.center.equalToSuperview()
        }
        separatorLineView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.height.equalTo(0.5)
        }
        tableView.snp.makeConstraints { make in
            make.top.equalTo(navigationBarView.snp.bottom)
            make.bottom.leading.trailing.equalToSuperview()
        }
    }
    
    @objc private func backAction() {
        responder?.tapBackButton()
    }
}

// MARK: - UITableViewDelegate

extension ShopListView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        responder?.tap(shop: shops[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        UIView()
    }
}

// MARK: - UITableViewDataSource

extension ShopListView: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        shops.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = shops[indexPath.row]
        let cell = ShopListTableViewCell()
        cell.configure(with: .init(image: model.image, title: model.name))
        return cell
    }
}

// MARK: - UITableViewDataSource

extension ShopListView {
    struct ViewModel {
        var shops: [Shop]
    }
    
    func configure(with viewModel: ViewModel) {
        self.shops = viewModel.shops
        tableView.reloadData()
    }
}
