//
//  MainViewController.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import UIKit

protocol ShopListViewControllerDelegate: AnyObject {
    func cityChoosed(city: String)
}

final class ShopListViewController: UIViewController {
    // MARK: - Subview Properties
    
    private var contentView = ShopListView()
    
    var model: ShopListModel?
    
    override func loadView() {
        super.loadView()
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.responder = self
        model?.loadShops()
    }
    
}

// MARK: - ShopListModelChangeProtocol

extension ShopListViewController: ShopListModelChangeProtocol {
    func loaded(shops: [Shop]) {
        contentView.configure(with: .init(shops: shops))
    }
}

// MARK: - ShopListViewResponder

extension ShopListViewController: ShopListViewResponder {
    func tap(shop: Shop) {
        let viewController = ShopInfoAssembly().assembly(shop: shop)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tapBackButton() {
        navigationController?.popViewController(animated: true)
    }
}
