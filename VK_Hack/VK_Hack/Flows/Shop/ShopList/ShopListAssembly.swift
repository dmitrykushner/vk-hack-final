//
//  MainAssembly.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

final class ShopListAssembly {
    init() {
    }
    
    func assembly() -> ShopListViewController {
        let model = ShopListModel()
        let viewController = ShopListViewController()
        viewController.model = model
        model.responder = viewController
        return viewController
    }
}
