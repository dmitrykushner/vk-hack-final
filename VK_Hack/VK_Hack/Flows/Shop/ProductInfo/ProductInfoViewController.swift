//
//  MainViewController.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import UIKit

protocol ProductInfoViewControllerDelegate: AnyObject {
}

final class ProductInfoViewController: UIViewController {
    // MARK: - Subview Properties
    
    private var contentView = ProductInfoView()
    
    var model: ProductInfoModel?
    
    override func loadView() {
        super.loadView()
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.responder = self
        model?.viewLoaded()
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
}

// MARK: - ProductInfoModelChangeProtocol

extension ProductInfoViewController: ProductInfoModelChangeProtocol {
    func show(product: Product) {
        contentView.configure(with: .init(product: product))
    }
}

// MARK: - ProductInfoViewResponder

extension ProductInfoViewController: ProductInfoViewResponder {
    func tapBackButton() {
        navigationController?.popViewController(animated: true)
    }
}
