//
//  MainModel.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

protocol ProductInfoModelChangeProtocol: AnyObject {
    func show(product: Product)
}

final class ProductInfoModel {
    
    weak var responder: ProductInfoModelChangeProtocol?
    
    private var product: Product?
    
    init(product: Product) {
        self.product = product
    }
    
    func viewLoaded() {
        guard let product = product else { return }
        responder?.show(product: product)
    }
}
