//
//  MainView.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import SnapKit
import GoogleMaps
import Then
import UIKit

protocol ProductInfoViewResponder: AnyObject {
    // Тап на кнопку бэк
    func tapBackButton()
}

final class ProductInfoView: UIView {
    // MARK: - Public Properties
    
    weak var responder: ProductInfoViewResponder?
    
    // MARK: - Subview Properties
    
    private lazy var navigationBarView = UIView()

    private lazy var titleLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        $0.textAlignment = .center
    }

    private lazy var backButton = UIButton().then {
        $0.addTarget(self, action: #selector(backAction), for: .touchUpInside)
        $0.setImage(Asset.backArrow.image, for: .normal)
    }

    private lazy var separatorLineView = UIView().then { $0.backgroundColor = .lightGray }
    
    private lazy var productImageView = UIImageView()
    
    private lazy var nameLabel = UILabel().then { $0.font = UIFont.systemFont(ofSize: 16) }
    
    private lazy var costLabel = UILabel().then { $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold) }
    
    private lazy var descriptionTextView = UITextView().then {
        $0.showsHorizontalScrollIndicator = false
        $0.showsVerticalScrollIndicator = false
        $0.font = UIFont.systemFont(ofSize: 18)
    }
    
    private lazy var addToPocketButton = UIButton().then {
        $0.setTitle("Добавить в избранное", for: .normal)
        $0.setTitleColor(.white, for: .normal)
        $0.backgroundColor = Asset.Color.blue.color
        $0.layer.cornerRadius = 8
        $0.addTarget(self, action: #selector(addToPocketButtonAction), for: .touchUpInside)
    }
    
    // MARK: - Private Properties
    
    private var product: Product?
    private var isSelected = false
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
    // MARK: - Private Methods
    
    private func commonInit() {
        backgroundColor = .white
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(navigationBarView)
        navigationBarView.addSubview(backButton)
        navigationBarView.addSubview(titleLabel)
        navigationBarView.addSubview(separatorLineView)
        addSubview(productImageView)
        addSubview(nameLabel)
        addSubview(costLabel)
        addSubview(descriptionTextView)
        addSubview(addToPocketButton)
    }
    
    private func makeConstraints() {
        navigationBarView.snp.makeConstraints { make in
            make.top.equalTo(safeAreaLayoutGuide.snp.top)
            make.leading.trailing.equalToSuperview()
            make.height.equalTo(44)
        }
        backButton.snp.makeConstraints { make in
            make.top.bottom.equalToSuperview().inset(8)
            make.leading.equalToSuperview().inset(16)
            make.width.equalTo(28)
        }
        titleLabel.snp.makeConstraints { make in
            make.leading.equalTo(backButton.snp.trailing).offset(12)
            make.center.equalToSuperview()
        }
        separatorLineView.snp.makeConstraints { make in
            make.bottom.leading.trailing.equalToSuperview()
            make.height.equalTo(0.5)
        }
        productImageView.snp.makeConstraints { make in
            make.top.equalTo(navigationBarView.snp.bottom).offset(1)
            make.leading.trailing.equalToSuperview()
            // Лень было разбираться, чего frame = 0... Делаю в последний день..
            make.height.equalTo(350)
        }
        nameLabel.snp.makeConstraints { make in
            make.top.equalTo(productImageView.snp.bottom).offset(12)
            make.leading.trailing.equalToSuperview().inset(16)
        }
        costLabel.snp.makeConstraints { make in
            make.top.equalTo(nameLabel.snp.bottom).offset(6)
            make.leading.trailing.equalToSuperview().inset(16)
        }
        descriptionTextView.snp.makeConstraints { make in
            make.top.equalTo(costLabel.snp.bottom).offset(16)
            make.bottom.equalTo(addToPocketButton.snp.top).inset(16)
            make.leading.trailing.equalToSuperview().inset(14)
        }
        addToPocketButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(24)
            make.leading.trailing.equalToSuperview().inset(16)
            make.height.equalTo(45)
        }
    }
    
    @objc private func backAction() {
        responder?.tapBackButton()
    }
    
    @objc private func addToPocketButtonAction() {
        isSelected = !isSelected
        addToPocketButton.setTitle(isSelected ? "Удалить из избранного" : "Добавить в избранное", for: .normal)
        addToPocketButton.setTitleColor(isSelected ? Asset.Color.blue.color : .white, for: .normal)
        addToPocketButton.backgroundColor = isSelected ? .lightGray : Asset.Color.blue.color
    }
}

// MARK: - Configurable

extension ProductInfoView {
    struct ViewModel {
        var product: Product
    }
    
    func configure(with viewModel: ViewModel) {
        self.product = viewModel.product
        titleLabel.text = viewModel.product.name
        productImageView.image = viewModel.product.image
        nameLabel.text = viewModel.product.name
        costLabel.text = viewModel.product.cost
        descriptionTextView.text = viewModel.product.description
    }
}
