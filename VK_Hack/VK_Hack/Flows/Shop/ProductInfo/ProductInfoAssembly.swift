//
//  MainAssembly.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

final class ProductInfoAssembly {
    init() {
    }
    
    func assembly(product: Product) -> ProductInfoViewController {
        let model = ProductInfoModel(product: product)
        let viewController = ProductInfoViewController()
        viewController.model = model
        model.responder = viewController
        return viewController
    }
}
