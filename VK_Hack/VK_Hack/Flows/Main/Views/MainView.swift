//
//  MainView.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import SnapKit
import GoogleMaps
import Then
import UIKit

protocol MainViewResponder: AnyObject {
    /// Тап на пин адидас
    func tapAdidas(shop: Shop)
    /// Тап на пин найк
    func tapNike(shop: Shop)
    /// Тап на пин гучи
    func tapGucci(shop: Shop)
    /// Тап на город
    func cityTap()
    /// Тап на магазин
    func shopTap()
}

final class MainView: UIView {
    // MARK: - Public Properties
    
    weak var responder: MainViewResponder?
    
    // MARK: - Subview Properties
    
    private lazy var mapView = GMSMapView()
    
    private lazy var cityLabel = UILabel().then {
        $0.font = UIFont.systemFont(ofSize: 18, weight: .semibold)
        $0.text = "Cанкт-Петербург"
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCity))
        $0.isUserInteractionEnabled = true
        $0.addGestureRecognizer(tapGesture)
    }
    
    private lazy var shopButton = UIButton().then {
        $0.contentMode = .scaleAspectFit
        $0.setImage(Asset.shop.image, for: .normal)
        $0.addTarget(self, action: #selector(tapShopButton), for: .touchUpInside)
        $0.backgroundColor = .white
        $0.layer.cornerRadius = 25
    }
    
    // MARK: - UIView
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }
    
    required init?(coder: NSCoder) { nil }
    
    // MARK: - Private Properties
    
    var shops: [Shop] = []
    
    // MARK: - Private Methods
    
    private func commonInit() {
        backgroundColor = .white
        mapView.delegate = self
        addSubviews()
        makeConstraints()
    }
    
    private func addSubviews() {
        addSubview(mapView)
        addSubview(cityLabel)
        addSubview(shopButton)
    }
    
    private func makeConstraints() {
        cityLabel.snp.makeConstraints { make in
            make.top.equalToSuperview().inset(50)
            make.centerX.equalToSuperview()
        }
        mapView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        shopButton.snp.makeConstraints { make in
            make.bottom.equalToSuperview().inset(40)
            make.trailing.equalToSuperview().inset(16)
            make.size.equalTo(50)
        }
    }
    
    private func imageWithImage(image: UIImage, scaledToSize newSize: CGSize) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(newSize, false, .zero)
        image.draw(in: CGRect(x: .zero, y: .zero, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
        UIGraphicsEndImageContext()
        return newImage
    }
    
    @objc private func tapCity() {
        responder?.cityTap()
    }
    
    @objc private func tapShopButton() {
        responder?.shopTap()
    }
    
    func configure(with shops: [Shop]) {
        self.shops = shops
        // Буду брать по координате пользователя и координате ближайшего магазина
        mapView.camera = GMSCameraPosition(latitude: 59.877902, longitude: 30.313022, zoom: 12)
        shops.forEach {
            let location = CLLocationCoordinate2D(latitude: $0.coordinate.latitude, longitude: $0.coordinate.longitude)
            let marker = GMSMarker(position: location)
            marker.icon = imageWithImage(image: $0.mapPin, scaledToSize: CGSize(width: 40, height: 40))
            marker.title = $0.name
            marker.map = mapView
            marker.isTappable = true
        }
    }
    
    func updateTitle(with city: String) {
        cityLabel.text = city
    }
}

// MARK: - GMSMapViewDelegate

extension MainView: GMSMapViewDelegate {
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        // Переделается на нормальное :)
        switch marker.title {
        case "Adidas":
            guard let shop = shops.first else { return true }
            responder?.tapAdidas(shop: shop)
        case "Nike":
            let shop = shops[1]
            responder?.tapNike(shop: shop)
        case "Gucci":
            guard let shop = shops.last else { return true }
            responder?.tapGucci(shop: shop)
        default:
            print(4)
        }
        return true
    }
}
