//
//  MainModel.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

protocol MoinModelChangeProtocol: AnyObject {
    func shopsCreated(shops: [Shop])
}

final class MainModel {
    
    weak var responder: MoinModelChangeProtocol?
    
    var shops: [Shop] = []
    
    init() {
    }
    
    func loadedShops() {
        shops = Database.createShops()
        responder?.shopsCreated(shops: shops)
    }
}
