//
//  MainAssembly.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import Foundation

final class MainAssembly {
    init() {
    }
    
    func assembly() -> MainViewController {
        let model = MainModel()
        let viewController = MainViewController()
        viewController.model = model
        model.responder = viewController
        return viewController
    }
}
