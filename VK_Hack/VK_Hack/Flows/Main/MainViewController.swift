//
//  MainViewController.swift
//  Mapper
//
//  Created by Dima Kushner Harbros on 10/24/20.
//

import UIKit

class MainViewController: UIViewController {
    // MARK: - Subview Properties
    
    private var contentView = MainView()
    
    var model: MainModel?
    
    override func loadView() {
        super.loadView()
        view = contentView
        model?.loadedShops()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        contentView.responder = self
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
}

// MARK: - MoinModelChangeProtocol

extension MainViewController: MoinModelChangeProtocol {
    func shopsCreated(shops: [Shop]) {
        contentView.configure(with: shops)
    }
}

// MARK: - MainViewResponder

extension MainViewController: MainViewResponder {
    func tapAdidas(shop: Shop) {
        let viewController = ShopInfoAssembly().assembly(shop: shop)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tapNike(shop: Shop) {
        let viewController = ShopInfoAssembly().assembly(shop: shop)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func tapGucci(shop: Shop) {
        let viewController = ShopInfoAssembly().assembly(shop: shop)
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func shopTap() {
        let viewController = ShopListAssembly().assembly()
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func cityTap() {
        let viewController = UINavigationController(rootViewController: CitiesAssembly().assembly(delegate: self))
        navigationController?.present(viewController, animated: true, completion: nil)
    }
}

// MARK: - CitiesViewControllerDelegate

extension MainViewController: CitiesViewControllerDelegate {
    func cityChoosed(city: String) {
        contentView.updateTitle(with: city)
    }
}
