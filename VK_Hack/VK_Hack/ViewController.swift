//
//  ViewController.swift
//  VK_Hack
//
//  Created by Dima Kushner Harbros on 10/31/20.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        let viewController = MainAssembly().assembly()
        navigationController?.pushViewController(viewController, animated: true)
    }


}

