// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal enum Adidas {
    internal static let adidasEightProduct = ImageAsset(name: "Adidas/adidas_eight_product")
    internal static let adidasFirstProduct = ImageAsset(name: "Adidas/adidas_first_product")
    internal static let adidasFivethProduct = ImageAsset(name: "Adidas/adidas_fiveth_product")
    internal static let adidasFourthProduct = ImageAsset(name: "Adidas/adidas_fourth_product")
    internal static let adidasLogo = ImageAsset(name: "Adidas/adidas_logo")
    internal static let adidasMapPin = ImageAsset(name: "Adidas/adidas_map_pin")
    internal static let adidasNinethProduct = ImageAsset(name: "Adidas/adidas_nineth_product")
    internal static let adidasSecondProduct = ImageAsset(name: "Adidas/adidas_second_product")
    internal static let adidasSeventhProduct = ImageAsset(name: "Adidas/adidas_seventh_product")
    internal static let adidasSixthProduct = ImageAsset(name: "Adidas/adidas_sixth_product")
    internal static let adidasThirdProduct = ImageAsset(name: "Adidas/adidas_third_product")
  }
  internal enum Color {
    internal static let blue = ColorAsset(name: "Color/blue")
  }
  internal enum Gucci {
    internal static let gucciEightProduct = ImageAsset(name: "Gucci/gucci_eight_product")
    internal static let gucciFirstProduct = ImageAsset(name: "Gucci/gucci_first_product")
    internal static let gucciFivethProduct = ImageAsset(name: "Gucci/gucci_fiveth_product")
    internal static let gucciFourthProduct = ImageAsset(name: "Gucci/gucci_fourth_product")
    internal static let gucciLogo = ImageAsset(name: "Gucci/gucci_logo")
    internal static let gucciMapPin = ImageAsset(name: "Gucci/gucci_map_pin")
    internal static let gucciNinethProduct = ImageAsset(name: "Gucci/gucci_nineth_product")
    internal static let gucciSecondProduct = ImageAsset(name: "Gucci/gucci_second_product")
    internal static let gucciSeventhProduct = ImageAsset(name: "Gucci/gucci_seventh_product")
    internal static let gucciSixthProduct = ImageAsset(name: "Gucci/gucci_sixth_product")
    internal static let gucciThirdProduct = ImageAsset(name: "Gucci/gucci_third_product")
  }
  internal enum Nike {
    internal static let nikeEightProduct = ImageAsset(name: "Nike/nike_eight_product")
    internal static let nikeFirstProduct = ImageAsset(name: "Nike/nike_first_product")
    internal static let nikeFivethProduct = ImageAsset(name: "Nike/nike_fiveth_product")
    internal static let nikeFourthProduct = ImageAsset(name: "Nike/nike_fourth_product")
    internal static let nikeLogo = ImageAsset(name: "Nike/nike_logo")
    internal static let nikeMapPin = ImageAsset(name: "Nike/nike_map_pin")
    internal static let nikeNinethProduct = ImageAsset(name: "Nike/nike_nineth_product")
    internal static let nikeSecondProduct = ImageAsset(name: "Nike/nike_second_product")
    internal static let nikeSeventhProduct = ImageAsset(name: "Nike/nike_seventh_product")
    internal static let nikeSixthProduct = ImageAsset(name: "Nike/nike_sixth_product")
    internal static let nikeThirdProduct = ImageAsset(name: "Nike/nike_third_product")
  }
  internal static let backArrow = ImageAsset(name: "backArrow")
  internal static let shop = ImageAsset(name: "shop")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = Color(asset: self)

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init!(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image named \(name).")
    }
    return result
  }
}

internal extension ImageAsset.Image {
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init!(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
